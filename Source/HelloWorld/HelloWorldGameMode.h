// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "HelloWorldGameMode.generated.h"

UCLASS(minimalapi)
class AHelloWorldGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AHelloWorldGameMode(const FObjectInitializer& ObjectInitializer);
};



